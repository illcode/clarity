#!/usr/bin/env python2

"""Null logger, returns None for any attribute lookup."""

class NullLogger():
    def __getattr__(self, item):
        return self.null

    def null(self, *args):
        return None
