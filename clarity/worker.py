#!/usr/bin/env python2

"""Background worker to remove outdated slides."""

import db
import time


def deleteOldSlides(test=False):
    d = db.Database(test=test)
    for slide in d.getSlideNames(time.time(), state="past"):
        d.deleteSlide(d.getIDFromKey(slide))
