#!/usr/bin/env python2

from bottle import abort, app, delete, error, get, post, run, request, response
import db
import api_errno
import json
import sys

class StripPathMiddleware(object):
    def __init__(self, app):
        self.app = app
    def __call__(self, e, h):
        e['PATH_INFO'] = e['PATH_INFO'].rstrip('/')
        return self.app(e, h)

@error(400)
def badArgs(error):
    response.content_type = "application/json"
    return json.dumps({
        "status":api_errno.BAD_ARGS,
    })

@error(404)
def notFound(error):
    response.content_type = "application/json"
    return json.dumps({
        "status":api_errno.BAD_ENDPOINT
    })

@error(405)
def notFound(error):
    response.content_type = "application/json"
    return json.dumps({
        "status":api_errno.BAD_METHOD
    })

@error(500)
def unhandledServerError(error):
    response.content_type = "application/json"
    return json.dumps({
        "status":api_errno.INTERNAL_ERROR
    })

@get("/slides/:state")
def getSlides(state):
    """
    curl http://localhost:8080/slides/all
    """
    return {
        "status":api_errno.OK,
        "slides":d.getSlides(state, honourExclusive=False)
    }

@delete("/slide/:slideID")
def deleteSlide(slideID):
    """
    curl -X DELETE http://localhost:8080/slide/0
    """
    d.deleteSlide(slideID)
    return {
        "status":api_errno.OK
    }

@get("/slide/:slideID")
def getSlide(slideID):
    """
    curl http://localhost:8080/slide/0
    """
    return {
        "status":api_errno.OK,
        "slide":d.getSlide(d.slide % slideID)
    }

@post("/slide")
def addSlide():
    """
    curl -d "filename=foo&data=YmFy&duration=10&exclusive=1&priority=100&start=1&end=2" http://localhost:8080/slide
    """

    try:
        slideID = d.addSlide(**request.forms)
    except (TypeError, KeyError):
        abort(400)

    if slideID != None:
        return {
            "status":api_errno.OK,
            "id":slideID
        }
    else:
        return {
            "status":api_errno.BAD_TIMES
        }

if __name__ == "__main__":
    if len(sys.argv) == 2 and sys.argv[1] == "--live":
        d = db.Database()
    else:
        print >> sys.stderr, "\nNote: running with test database, use --live if you want the live one\n"
        d = db.Database(test=True)

    api = StripPathMiddleware(app())
    run(app=api, host='0.0.0.0', port=8080)
