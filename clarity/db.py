#!/usr/bin/env python2

"""Get information from the Redis database."""

import base64
import nulllogger
import redis
import time
import logging as l

class UnknownStateError(Exception):
    """Raised when an invalid slide state is passed."""
    pass

class Database():
    def __init__(self, test=False):
        if test:
            self.r = redis.Redis(port=6378)
            self.l = nulllogger.NullLogger()
        else:
            self.r = redis.Redis()
            self.l = l

        self.keyspace = "clarity"
        self.currentID = "%s:id" % self.keyspace
        self.start = "%s:start" % self.keyspace
        self.end = "%s:end" % self.keyspace
        self.slide = "%s:slide:%%s" % self.keyspace

    def sortByPriority(self, data):
        """Sort by each dict's priority key."""
        return sorted(data, key=lambda x: int(x["priority"]))

    def getIDByFilename(self, filename):
        """Warning, slow."""
        for slide in self.getSlides(state="all"):
            if slide["filename"] == filename:
                yield int(slide["id"])

    def unmangleSlide(self, slide):
        """Convert Redis strings to the data formats that we want."""
        for key in ("priority", "duration"):
            slide[key] = int(slide[key])
        slide["is_exclusive"] = slide["is_exclusive"] != "0"
        return slide

    def getSlideNames(self, now, state="current"):
        """Get all of the slide names matching a criteria."""
        if state == "current":
            started = set(self.r.zrangebyscore(self.start, "-inf", now))
            notEnded = set(self.r.zrangebyscore(self.end, now, "+inf"))
            return started & notEnded
        elif state == "future":
            return set(self.r.zrangebyscore(self.start, now + 1, "+inf"))
        elif state == "past":
            return set(self.r.zrangebyscore(self.start, "-inf", now - 1))
        elif state == "all":
            return set(self.r.zrangebyscore(self.start, "-inf", "+inf"))
        else:
            raise UnknownStateError(state)

    def getIDFromKey(self, key):
        return int(key.split(":", 2)[2])

    def getSlide(self, slideName):
        slide = self.r.hgetall(slideName)
        start = int(self.r.zscore(self.start, slideName))
        end = int(self.r.zscore(self.end, slideName))
        slide.update({
            "id":self.getIDFromKey(slideName),
            "start":start,
            "end":end
        })
        return self.unmangleSlide(slide)

    def getSlides(self, state="current", honourExclusive=True):
        """Get all of the hashes for slide names matching a criteria."""
        slides = []
        slideNames = self.getSlideNames(time.time(), state)
        for slideName in slideNames:
            slides.append(self.getSlide(slideName))

        if state == "current" and honourExclusive:
            exclusiveSlides = [ x for x in slides if x["is_exclusive"] ]
            if exclusiveSlides:
                return self.sortByPriority(exclusiveSlides)
        return self.sortByPriority(slides)

    def addSlide(self, filename, data, duration, is_exclusive, priority, start, end):
        """Add a slide to Redis."""
        slideID = self.r.incr(self.currentID)
        slide = self.slide % slideID

        try:
            start = int(start)
        except ValueError:
            self.l.warning("Denied slide, invalid start time (%s): %s" % (start, slide))
            return False
        try:
            end = int(end)
        except ValueError:
            self.l.warning("Denied slide, invalid end time (%s): %s" % (end, slide))
            return False

        if start > end:
            self.l.warning("Denied slide, older start time than end time: %s" % slide)
            return False

        maps = {
            "start":{ slide:start },
            "end":{ slide:end }
        }

        if not self.r.zadd(self.start, **maps["start"]):
            self.l.info("Start time already exists, updated: %s" % slide)
        if not self.r.zadd(self.end, **maps["end"]):
            self.l.info("End time already exists, updated: %s" % slide)

        pairs = (
            ("filename", filename),
            ("data", base64.b64encode(data)),
            ("duration", duration),
            ("is_exclusive", int(is_exclusive)),
            ("priority", priority),
        )

        p = self.r.pipeline()
        for k, v in pairs:
            p.hset(slide, k, v)
        if not all(p.execute()):
            self.l.info("Key already exists, updated: %s" % slide)
        return slideID

    def deleteSlide(self, slideID):
        """Delete a slide from Redis."""
        slide = self.slide % slideID

        p = self.r.pipeline()

        p.delete(slide)
        p.zrem(self.start, slide)
        p.zrem(self.end, slide)

        ret = p.execute()

        if not any(ret):
            self.l.warning("Deletion of nonexistent slide (database out of sync?): %s" % slide)
            return False
        elif not all(ret):
            self.l.warning("Deletion of inconsistent slide (database out of sync?): %s" % slide)
        return True
