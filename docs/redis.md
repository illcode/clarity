Initial metadata for servers:

- clarity:defaults
    - :duration
        - The default duration for slides

For each slide we create a hash as follows:

- clarity:slide:[filename]
    - :data
        - The image data, encoded in base64
    - :duration
        - The duration of the slide, or null if client default should be used
    - :is\_exclusive
        - True if slide is exclusive, or False
    - :priority
        - Determines in what order the slides are shown. Higher = closer to the
          end of the queue

There are two sorted sets that designate the start/end time of the slide, these
are at `clarity:start` and `clarity:end`, respectively. The score is the time
to start or stop displaying the slide.
