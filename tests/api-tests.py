#!/usr/bin/env python2

import os
import unittest
import httplib
import urllib
import base64
import json
import sys
sys.path.append(os.path.join(os.path.dirname(__file__), "../clarity"))
import db

c = httplib.HTTPConnection("localhost:8080")
r = db.Database(test=True).r

class APIUnitTests(unittest.TestCase):
    def setUp(self):
        r.flushdb()
        self.data = {
            "filename":"foo",
            "data":open(os.path.join(os.path.dirname(__file__), "files/colormap.jpg")).read(),
            "duration":10,
            "is_exclusive":1,
            "priority":100,
            "start":1,
            "end":2
        }

    def testCreate(self):
        dataSend = self.data.copy()

        params = urllib.urlencode(dataSend)
        headers = {
            "Content-Type": "application/x-www-form-urlencoded",
            "Accept": "application/json"
        }
        c.request("POST", "/slide", params, headers)
        res = c.getresponse()
        self.assertTrue(res.status == 200)
        slideID = json.load(res)["id"]

        c.request("GET", "/slide/%d" % slideID)
        res = c.getresponse()

        newData = json.load(res)["slide"]
        del newData["id"]
        newData["data"] = base64.b64decode(newData["data"])

        self.assertTrue(self.data == newData)

    def testMultiSlideRetrieval(self):
        self.testCreate()
        c.request("GET", "/slides/all")
        res = c.getresponse()

        data = json.load(res)["slides"]

        self.assertTrue(len(data) == 1)

        newData = data[0]
        del newData["id"]
        newData["data"] = base64.b64decode(newData["data"])

        self.assertTrue(self.data == newData)

    def testMissingArgsReturns400(self):
        dataSend = self.data.copy()
        del dataSend["is_exclusive"]

        params = urllib.urlencode(dataSend)
        headers = {
            "Content-Type": "application/x-www-form-urlencoded",
            "Accept": "application/json"
        }
        c.request("POST", "/slide", params, headers)
        res = c.getresponse()
        self.assertTrue(res.status == 400)

    def testBadRouteReturns404(self):
        c.request("GET", "/nonexistent")
        res = c.getresponse()
        self.assertTrue(res.status == 404)

    def testBadMethodReturns405(self):
        c.request("POST", "/slides/all")
        res = c.getresponse()
        self.assertTrue(res.status == 405)

if __name__ == "__main__":
    unittest.main()
