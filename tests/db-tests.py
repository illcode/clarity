#!/usr/bin/env python2
# coding: utf8

import base64
import unittest
import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__), "../clarity"))
import db
import worker
import time


class DatabaseUnitTests(unittest.TestCase):
    def setUp(self):
        self.d = db.Database(test=True)
        self.d.r.flushdb()
        self.d.addSlide("past", "past", 10, False, 80, time.time() - 60, time.time() - 1)
        self.d.addSlide("future", "future", 10, False, 100, time.time() + 60, time.time() + 3660)
        self.d.addSlide("current", "current", 10, False, 90, time.time(), time.time() + 60)

    def testExclusivity(self):
        self.d.addSlide("currentExclusive", "currentExclusive", 10, True, 100, time.time(), time.time() + 60)
        exclusiveSlides = {
            "current":self.d.getSlides(state="current"),
            "all":self.d.getSlides(state="all")
        }
        self.assertTrue(len(exclusiveSlides["current"]) == 1)
        exclusiveSlides["current"][0]["data"] = base64.b64decode(exclusiveSlides["current"][0]["data"])
        self.assertTrue(exclusiveSlides["current"][0]["data"] == "currentExclusive")
        self.assertTrue(len(exclusiveSlides["all"]) == 4)

    def testWorkerRemovesOldSlides(self):
        slides = list(self.d.getSlides(state="past"))
        self.assertTrue(len(slides) == 1, msg="Bad number of past slides before worker")

        worker.deleteOldSlides(test=True)

        slides = list(self.d.getSlides(state="past"))
        self.assertTrue(len(slides) == 0, msg="Bad number of past slides after worker")

    def testFilenameWithColon(self):
        self.d.addSlide("filename:with:colon", "filename:with:colon", 10, True, 100, time.time(), time.time() + 60)
        slides = [ x for x in self.d.getSlides(state="all") if x["filename"] == "filename:with:colon" ]
        self.assertTrue(slides)

    def testFilenameUnicode(self):
        self.d.addSlide("їłℓ¢☺∂ℯ", "їłℓ¢☺∂ℯ", 10, True, 100, time.time(), time.time() + 60)
        slides = [ x for x in self.d.getSlides(state="all") if x["filename"] == "їłℓ¢☺∂ℯ" ]
        self.assertTrue(slides)

    def testPrioritySorting(self):
        slides = list(self.d.getSlides(state="all"))
        for data in ("future", "current", "past"):
            self.assertTrue(slides.pop()["data"] == base64.b64encode(data))

    def testCannotAddNonIntegerTimes(self):
        self.assertFalse(self.d.addSlide("mangled", "mangled", 10, False, 100, "garbage", time.time()))
        self.assertFalse(self.d.addSlide("mangled", "mangled", 10, False, 100, time.time(), "garbage"))

    def testCannotAddInverseTimes(self):
        self.assertFalse(self.d.addSlide("mangled", "mangled", 10, False, 100, time.time() + 1, time.time()))

    def testRealImage(self):
        colormaps = {
            "jpg":open(os.path.join(os.path.dirname(__file__), "files/colormap.jpg")).read(),
            "png":open(os.path.join(os.path.dirname(__file__), "files/colormap.png")).read()
        }

        for name in ("jpg", "png"):
            self.assertTrue(self.d.addSlide(name, colormaps[name], 10, False, 100, time.time(), time.time()))

        slides = list(self.d.getSlides(state="all"))

        for name in ("jpg", "png"):
            slide = list([ x for x in slides if x["filename"] == name ])[0]
            self.assertTrue(slide["data"] == base64.b64encode(colormaps[name]))

    def testDefaultState(self):
        self.assertTrue(list(self.d.getSlides()) == list(self.d.getSlides(state="current")), msg="Default state is not current")

    def testAllSlides(self):
        slides = list(self.d.getSlides(state="all"))
        self.assertTrue(len(slides) == 3, msg="Bad number of total slides")

    def testNumberOfSlides(self):
        slides = list(self.d.getSlides(state="current"))
        self.assertTrue(len(slides) == 1, msg="Bad number of current slides")

    def testPastSlide(self):
        slides = list(self.d.getSlides(state="past"))
        self.assertTrue(len(slides) == 1, msg="Bad number of past slides")
        slides[0]["data"] = base64.b64decode(slides[0]["data"])
        self.assertTrue(slides[0]["data"] == "past", msg="Past slide not returned")

    def testCurrentSlide(self):
        slides = list(self.d.getSlides(state="current"))
        self.assertTrue(len(slides) == 1, msg="Bad number of current slides")
        slides[0]["data"] = base64.b64decode(slides[0]["data"])
        self.assertTrue(slides[0]["data"] == "current", msg="Present slide not returned")

    def testFutureSlide(self):
        slides = list(self.d.getSlides(state="future"))
        self.assertTrue(len(slides) == 1, msg="Bad number of future slides")
        slides[0]["data"] = base64.b64decode(slides[0]["data"])
        self.assertTrue(slides[0]["data"] == "future", msg="Future slide not returned")

    def testDelete(self):
        slides = self.d.getSlides(state="all")
        slideID = list(self.d.getIDByFilename("future"))
        self.assertTrue(len(slideID) == 1)
        slideID = slideID[0]
        self.assertTrue(self.d.deleteSlide(slideID))
        self.assertFalse(self.d.deleteSlide(slideID))

if __name__ == "__main__":
    unittest.main()
